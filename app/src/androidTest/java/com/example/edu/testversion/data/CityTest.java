package com.example.edu.testversion.data;

import junit.framework.TestCase;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class CityTest extends TestCase {

    public void testGetGeographicalString() throws Exception {
        City city = new City("Vancouver", "BC", "CA");
        assertThat("Geographical string is not recognizable",
                city.getGeographicalString(),
                is("Vancouver, BC, CA"));
    }


}