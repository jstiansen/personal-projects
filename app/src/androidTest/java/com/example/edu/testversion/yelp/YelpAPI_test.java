package com.example.edu.testversion.yelp;

import android.test.ActivityInstrumentationTestCase2;

import com.example.edu.testversion.activity.CategoryChooser;
import com.example.edu.testversion.activity.YelpGameApplication;

/**
 * Created by Jonathan Stiansen on 2014-08-31.
 */
public class YelpAPI_test extends ActivityInstrumentationTestCase2<CategoryChooser> {

    private YelpAPI yelpApi;

    public YelpAPI_test() {
        super(CategoryChooser.class);
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();
    }

    public void testInitialConditions() throws Exception {
        yelpApi = YelpGameApplication.getYelpApi();
        assertNotNull(yelpApi);
    }

//    public void testPlayButtonWorks(){
//        YelpAPI mockAPI = mock(YelpAPI.class);
//        YelpGameApplication.setYelpAPI(mockAPI);
//        when(mockAPI.queryAPI(anyString(),anyString(),anyString(),anyInt())).thenReturn(new JSONObject());
//
//        CategoryChooser categoryChooser = getActivity();
//        CategoryChooserTest.startUpCategoryGame(categoryChooser.getApplicationContext());
//
//        verify(mockAPI, times(1)).queryAPI(anyString(),anyString(),anyString(),anyInt());
//    }
}
