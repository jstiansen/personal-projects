package com.example.edu.testversion.data;

import junit.framework.TestCase;
import static com.google.android.apps.common.testing.ui.espresso.matcher.ViewMatchers.assertThat;
import static org.hamcrest.core.Is.is;

public class ReviewTest extends TestCase {

    public void setUp() throws Exception {
        super.setUp();
    }

    public void tearDown() throws Exception {

    }

    public void testGetReview() throws Exception {
        String review = "I really like this place.";
        Review r = new Review("XSDFE", 1, review, 12355555, "WWW.COM");
        assertTrue(r.getReview().equals(review));
    }

    public void testHasKeyword() throws Exception {
        String review = "I like. Lots. I can't wait to go, back";
        Review r = new Review("ASDGFG", 1, review, 12355555, "WWW.COM");
        assertThat("Parsed space incorrectly", r.hasKeyword(" "), is(0));
        assertThat("Parsed space and period incorrectly", r.hasKeyword(". "), is(0));
        assertThat("Parsed 'I' incorrectly", r.hasKeyword("I"), is(2));
        assertThat("Parsed 'can't' incorrectly", r.hasKeyword("can't"), is(1));
    }
}