package com.example.edu.testversion.activity;

import com.example.edu.testversion.data.Category;
import com.example.edu.testversion.data.Restaurant;
import com.example.edu.testversion.yelp.YelpDataParser;
import com.example.edu.testversion.yelp.YelpDataParserTest;
import com.example.edu.testversion.yelp.exceptions.ConversionException;

import org.json.simple.parser.ParseException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jonathan Stiansen on 2014-08-28.
 */
public class TestingHelpers {

    public static ArrayList<Category> getTestCategories() {
        ArrayList<Category> cats = new ArrayList<Category>();
        cats.add(new Category("Indian", "indpak"));
        cats.add(new Category("Pakistani",
                "pakistani"));
        return cats;
    }

    public static List<Restaurant> getRestaurants() throws ParseException, ConversionException {
        YelpDataParser yelpDataParser = new YelpDataParser();
        return YelpDataParserTest.PublicJSONRestaurant.getFullRestaurantJson(yelpDataParser);
    }
}
