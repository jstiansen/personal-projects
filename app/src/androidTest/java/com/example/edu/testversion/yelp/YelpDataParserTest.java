package com.example.edu.testversion.yelp;

import android.util.Log;

import com.example.edu.testversion.data.Category;
import com.example.edu.testversion.data.City;
import com.example.edu.testversion.data.GeoArea;
import com.example.edu.testversion.data.Neighborhood;
import com.example.edu.testversion.data.Restaurant;
import com.example.edu.testversion.data.Review;
import com.example.edu.testversion.yelp.exceptions.ConversionException;

import junit.framework.TestCase;

import org.hamcrest.Matchers;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static android.test.MoreAsserts.assertEmpty;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.core.Is.is;

public class YelpDataParserTest extends TestCase {

    private static final String TAG = "Test_JsonToJavaConverter";
    private YelpDataParser mConverter;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        mConverter = new YelpDataParser();
    }

    @Override
    public void tearDown() throws Exception {
        super.tearDown();
    }

    public void testDoesNotParseClosedBusiness() throws Exception {

        String jsonClosedBusiness = PublicJSONRestaurant.getFullRestaurantJson(true);
        JSONObject jsonObject = mConverter.parseObject(jsonClosedBusiness);
        List<Restaurant> actualRestaurantList = mConverter.parseRestaurantData(jsonObject);

        assertEmpty(actualRestaurantList);
    }

    public void testParseRestaurantData() throws Exception {
        String jsonBusiness = PublicJSONRestaurant.getFullRestaurantJson(false);
        List<Restaurant> expectedRestaurants = PublicJSONRestaurant.getFullRestaurantJson(mConverter);
        JSONObject jsonObject = mConverter.parseObject(jsonBusiness);

        List<Restaurant> actualRestaurants = mConverter.parseRestaurantData(jsonObject);
        assertThat("Not working so well", actualRestaurants.get(0), equalTo(expectedRestaurants.get(0)));
        //assertThat(actualRestaurants, hasItems(expectedRestaurants));
    }

    public void testInitialConditions() throws Exception {
        assertNotNull("The YelpDataParser has not been properly instantiated", mConverter);
    }

    public void testParsesSingleCategory_works() throws Exception {
        String cat1 = "Japanese";
        String alias1 = "japanese";
        String jsonCategories = "{\"arr\":[[\"" + cat1 + "\", \"" + alias1 + "\"]]}";
        Category testCat1 = new Category(cat1, alias1);

        JSONObject catJsonObj = mConverter.parseObject(jsonCategories);
        Collection<Category> actualResults = mConverter.convertCategories((JSONArray) catJsonObj.get("arr"));

        assertTrue("parsedCategories don't contain Japanese category, has " + actualResults.iterator().next().getName(), actualResults.contains(testCat1));
    }

    public void testParsesMultipleCategories_works() throws Exception {
        String cat1 = "Japanese";
        String alias1 = "japanese";
        String cat2 = "Tapas Bars";
        String alias2 = "tapas";
        String jsonCategories = "{\"arr\":[[\"" + cat1 + "\", \"" + alias1 + "\"], [\"" + cat2 + "\", \"" + alias2 + "\"]]}";
        Category testCat0 = new Category(cat1, alias1);
        Category testCat1 = new Category(cat2, alias2);

        JSONObject catJsonObj = mConverter.parseObject(jsonCategories);
        Collection<Category> actualResults = mConverter.convertCategories((JSONArray) catJsonObj.get("arr"));
        // This is a convoluted way to assert that the collection contains both categories.
        // This is because the method on collections '.contains' isn't calling the categories equal method

        assertTrue("parsedCategories don't contain both contained categories, was: " + actualResults, actualResults.contains(testCat0) && actualResults.contains(testCat1));
    }

    public void testParseCategories_throws() throws Exception {
        String badString = "{\"arr\": [\"No category array inside outer array\"]}";
        JSONObject jsonObject = mConverter.parseObject(badString);

        try {
            mConverter.convertCategories((JSONArray) jsonObject.get("arr"));
            fail("Missing exception" +
                    "");
        } catch (ConversionException pe) {
            assertTrue("Message was: " + pe.getMessage(), pe.getMessage().contains("Category array did not contain other category arrays"));
        }
    }

    public void testConvertReviews() throws Exception {
        int rating = 5;
        String excerpt = "I am completely dying over my meal here. Two words: dy.ing. Thus, I don't think there is a more fitting business worthy of my 200th Yelp review than Guu...";
        long timeCreated = 1407129573;
        String ratingImageURL = "http://s3-media1.fl.yelpcdn.com/assets/2/www/img/c7623205d5cd/ico/stars/v1/stars_small_5.png";
        String id = "dXEfc5bvUgqukf7dRjACdg";
        String restaurantString = PublicJSONRestaurant.getFullRestaurantJson(false);
        JSONObject jsonObject = mConverter.parseObject(restaurantString);
        JSONObject restaurantJSON = (JSONObject) ((JSONArray) jsonObject.get("businesses")).get(0);
        JSONArray reviewsJSON = (JSONArray) restaurantJSON.get("reviews");
        assertNotNull("No reviews field in the json object", reviewsJSON);
        List<Review> reviews = mConverter.convertReviews(reviewsJSON);

        Review r = new Review(id, rating, excerpt, timeCreated, ratingImageURL);

        assertThat(r, is(reviews.get(0)));


    }

    public void testNeighborhoodConversion_works() throws Exception {
        String cityName = "Vancouver";
        String neighbourhood = "West End";
        String country = "CA";
        String state = "BC";
        String locationString = " {\"city\": \"" + cityName + "\", \"display_address\": [\"1698 Robson Street\", \"West End\", \"Vancouver, BC V6G 1C7\", \"Canada\"], \"neighborhoods\": [\"" + neighbourhood + "\"], \"postal_code\": \"V6G 1C7\", \"country_code\": \"" + country + "\", \"address\": [\"1698 Robson Street\"], \"state_code\": \"" + state + "\"}";
        City expectedCity = new City(cityName,state, country);
        Neighborhood neighborhood = new Neighborhood(neighbourhood, expectedCity);

        JSONObject jsonObject = mConverter.parseObject(locationString);
        GeoArea area = mConverter.convertGeoArea(jsonObject);

        assertThat("It didn't properly convert to neighbourhood", area, Matchers.is(Neighborhood.class));
        assertThat("Geolocation didn't turn out to be an equal neighborhood", (Neighborhood) area, equalTo(neighborhood));
    }

    public void testCityConversion_works() throws Exception {
        String cityName = "Vancouver";
        String country = "CA";
        String state = "BC";
        String locationString = " {\"city\": \"" + cityName + "\", \"display_address\": [\"1698 Robson Street\", \"West End\", \"Vancouver, BC V6G 1C7\", \"Canada\"], \"neighborhoods\": [], \"postal_code\": \"V6G 1C7\", \"country_code\": \"" + country + "\", \"address\": [\"1698 Robson Street\"], \"state_code\": \"" + state + "\"}";
        City expectedCity = new City(cityName,state, country);

        JSONObject jsonObject = mConverter.parseObject(locationString);
        GeoArea area = mConverter.convertGeoArea(jsonObject);

        assertThat("Geolocation didn't turn out to be an equal city", (City) area, equalTo(expectedCity));
    }

    public void testParseWorksWithBool() throws Exception {
        String jsonBool = "{\"bool\": false}";

        JSONObject obj = mConverter.parseObject(jsonBool);
        Boolean bool = (Boolean) obj.get("bool");

        assertFalse("Boolean was not parsed properly should be false but was " + obj.get("bool"), bool);
    }

    public void testParseWorksWithObject() throws Exception {
        String jsonObj = "{\"review_count\": 361, \"name\": \"Guu with Garlic\"}";

        JSONObject obj = mConverter.parseObject(jsonObj);
        Object review_count = obj.get("review_count");
        if (!(review_count instanceof Number))
            fail("review_count is not an instance of number, instead it is: " + review_count.getClass());
        Object name = obj.get("name");

        assertTrue("Review count object is: " + review_count + " and expected the number 361", ((Number) review_count).intValue() == 361);
        assertEquals(name, "Guu with Garlic");
    }

    public void testParseWorksWithArray() throws Exception {
        String jsonArray = "{\"arr\":[\"bool\", \"hello\"]}";

        JSONObject obj = mConverter.parseObject(jsonArray);
        Log.d(TAG, obj.toJSONString());
        JSONArray arr = (JSONArray) obj.get("arr");

        assertTrue("Array was not parsed properly should have \"bool\" and \"hello\" in array " +
                "instead had: " + arr.toJSONString(), arr.contains("bool") && arr.contains("hello"));

    }

    public void testParseWorksWithString() throws Exception {
        String str_value = "string_value";
        String jsonBool = "{\"string\": \"" + str_value + "\"}";

        JSONObject obj = mConverter.parseObject(jsonBool);
        String string = (String) obj.get("string");

        assertTrue("String was not parsed properly should be" + str_value + " but was " + obj.get("string"), string.equals(str_value));
    }

    public static class PublicJSONRestaurant {
        public static final String NAME = "Guu with Garlic";
        public static final String CATEGORIES_STRING = "[[\"Japanese\", \"japanese\"], [\"Tapas Bars\", \"tapas\"]]";
        public static final String ID = "guu-with-garlic-vancouver-2";
        public static final String LOCATION_STRING =
                "{\"city\": \"Vancouver\", " +
                        "\"display_address\": [\"1698 Robson Street\", \"West End\", \"Vancouver, BC V6G 1C7\", \"Canada\"], " +
                        "\"neighborhoods\": [\"West End\"], " +
                        "\"postal_code\": \"V6G 1C7\", " +
                        "\"country_code\": \"CA\", " +
                        "\"address\": [\"1698 Robson Street\"], " +
                        "\"state_code\": \"BC\"}";
        public static final String REVIEWS = "\t\t\"reviews\": [{\"rating\": 5, \"excerpt\": \"I am completely dying over my meal here. Two words: dy.ing. Thus, I don't think there is a more fitting business worthy of my 200th Yelp review than Guu...\", \"time_created\": 1407129573, \"rating_image_url\": \"http://s3-media1.fl.yelpcdn.com/assets/2/www/img/f1def11e4e79/ico/stars/v1/stars_5.png\", \"rating_image_small_url\": \"http://s3-media1.fl.yelpcdn.com/assets/2/www/img/c7623205d5cd/ico/stars/v1/stars_small_5.png\", \"user\": {\"image_url\": \"http://s3-media3.fl.yelpcdn.com/photo/lgXQXzIsxgA2pBexHBUrcg/ms.jpg\", \"id\": \"JGnqasIkGMLfSejt7mw9qQ\", \"name\": \"Judy H.\"}, \"rating_image_large_url\": \"http://s3-media3.fl.yelpcdn.com/assets/2/www/img/22affc4e6c38/ico/stars/v1/stars_large_5.png\", \"id\": \"dXEfc5bvUgqukf7dRjACdg\"}]";

        public static String getFullRestaurantJson(boolean isClosed) {

            return "{\n" +
                    "\"businesses\": [" +
                    //Business #1
                    "{\"is_claimed\": true, \"rating\": 4.5, \"mobile_url\":\n" +
                    "\t\t\"http://m.yelp.ca/biz/" + ID + "\", \n" +
                    "\t\t\"rating_img_url\": \"http://s3-media2.fl.yelpcdn.com/assets/2/www/img/99493c12711e/ico/stars/v1/stars_4_half.png\", \n" +
                    "\t\t\"review_count\": 361, \"name\": \"" + NAME + "\", \n" +
                    "\t\t\"snippet_image_url\": \"http://s3-media2.fl.yelpcdn.com/photo/WlW5r36bnhTJbgYPuB8lmw/ms.jpg\", \n" +
                    "\t\t\"rating_img_url_small\": \"http://s3-media2.fl.yelpcdn.com/assets/2/www/img/a5221e66bc70/ico/stars/v1/stars_small_4_half.png\", \n" +
                    "\t\t\"url\": \"http://www.yelp.ca/biz/" + ID + "\", \n" +
                    REVIEWS + ", \n" +
                    "\t\t\"phone\": \"6046858678\", \"snippet_text\": \"1 word: YUM\\n2 words: OMG YUM\\n\\nOrder one of everything ... it is all delicious\", \"image_url\": \"http://s3-media2.fl.yelpcdn.com/bphoto/7wzBRUsOpePOT-6Bq8IiSw/ms.jpg\", \n" +
                    "\t\t\"" + "categories\": " + CATEGORIES_STRING + ", \n" +
                    "\t\t\"display_phone\": \"+1-604-685-8678\", \n" +
                    "\t\t\"rating_img_url_large\": \"http://s3-media4.fl.yelpcdn.com/assets/2/www/img/9f83790ff7f6/ico/stars/v1/stars_large_4_half.png\", \n" +
                    "\t\t\"id\": \"" + ID + "\", \n" +
                    "\t\t\"is_closed\": " + isClosed + ", \n" +
                    "\t\t\"" + "location\": " + LOCATION_STRING
                    + "}"
                    //End business
                    + " ],\n" +
                    "  \"region\": {\n" +
                    "    \"center\": {\n" +
                    "      \"latitude\": 37.786138600000001,\n" +
                    "      \"longitude\": -122.40262130000001\n" +
                    "    },\n" +
                    "    \"span\": {\n" +
                    "      \"latitude_delta\": 0.0,\n" +
                    "      \"longitude_delta\": 0.0\n" +
                    "    }\n" +
                    "  },\n" +
                    "  \"total\": 10651\n" +
                    "}"
                    ;
        }

        public static List<Restaurant> getFullRestaurantJson(YelpDataParser parser) throws ParseException, ConversionException {
            JSONObject jsonObject = parser.parseObject("{\"arr\":" + CATEGORIES_STRING + "}");
            JSONArray jsonCats = (JSONArray) jsonObject.get("arr");
            Collection<Category> categories = parser.convertCategories(jsonCats);
            JSONObject locationObj = parser.parseObject(LOCATION_STRING);
            ArrayList<Restaurant> restaurants = new ArrayList<Restaurant>();

            String cityName = (String) locationObj.get("city");
            String stateCode = (String) locationObj.get("state_code");
            String countryCode = (String) locationObj.get("country_code");
            City city = new City(cityName, stateCode, countryCode);
            // Neighbourhood
            JSONArray neighborhoodsArr = (JSONArray) locationObj.get("neighborhoods");
            String neighborhoodName = (String) neighborhoodsArr.get(0);
            Neighborhood neighborhood = new Neighborhood(neighborhoodName, city);
            // Postal Code
            String postalCode = (String) locationObj.get("postal_code");
            // Street Address
            JSONArray addressArr = (JSONArray) locationObj.get("address");
            String address = (String) addressArr.get(0);
            //Reviews
            String reviewString = "{"+REVIEWS+" }";
            JSONArray reviewsObj = (JSONArray) parser.parseObject(reviewString).get("reviews");
            List<Review> reviews = parser.convertReviews(reviewsObj);

            restaurants.add(new Restaurant(NAME, categories, ID, address, postalCode, neighborhood, reviews));
            return restaurants;
        }
    }


}
