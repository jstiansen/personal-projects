package com.example.edu.testversion.activity;

import android.content.Context;
import android.location.Geocoder;
import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;

import com.example.edu.testversion.R;
import com.example.edu.testversion.testUtils.Primer;
import com.google.android.apps.common.testing.ui.espresso.ViewInteraction;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

import junit.framework.Assert;

import static com.google.android.apps.common.testing.ui.espresso.Espresso.onView;
import static com.google.android.apps.common.testing.ui.espresso.matcher.ViewMatchers.withId;

/**
 * This is specifically for testing the map and its functions, this is not for unit testing,
 * its meant for functional testing
 * Created by Jonathan Stiansen on 2014-08-22.
 */
public class MapTesting extends ActivityInstrumentationTestCase2<CategoryChooser> {
    // Constant values, valid as of Aug 2014
    private static final int MIN_BEARING = 0;
    private static final int MAX_BEARING = 360;
    private static final int MIN_TILT = 0;
    private static final int MAX_TILT = 90;
    private static final String TAG = "TEST_CategoryChooser_map";
    // This DELTA is our margin of error. With 'float' values, we often can't do '==' since they
    // aren't EXACTLY the same, off my maybe a few million'ths
    private static final double DELTA = .01;
    private CategoryChooser mChooser;
    private Context mContext;
    private GoogleMap mMap;

    private static final LatLng VANCOUVER = new LatLng(49.2839499, -123.12008679999997);
    private static final LatLng REVOLVER = new LatLng(49.28319, -123.109344);

    public MapTesting() {
        super(CategoryChooser.class);
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();
        mChooser = getActivity();
        mMap = ((MapFragment) mChooser.getFragmentManager().findFragmentById(R.id.map))
                .getMap();
        mContext = mChooser.getApplicationContext();
        //This will 'prime' the android app and stop animations which is neccessary for espresso tests.
        Primer turnOffAnimationsPrimer = new Primer();
    }

    public void testInitialConditions() throws Exception {
        assertNotNull("Chooser activity under test is null!", mChooser);
        assertNotNull("Map was not initialized properly", mMap);
        assertTrue("Geocoder is not present currently", Geocoder.isPresent());
    }

    public void testMapCenteredOnVancouver() throws Exception {
        ViewInteraction vi = onView(withId(R.id.map));
        CameraPosition cp = mMap.getCameraPosition();
        LatLng focus = cp.target;
        double margin = VANCOUVER.latitude - focus.latitude;
        assertTrue("Latitude differed as:" + focus.latitude + " vs. " + VANCOUVER.latitude, margin < DELTA);
        margin = VANCOUVER.longitude - focus.longitude;
        assertTrue("Longitude differed as:" + focus.longitude + " vs. " + VANCOUVER.longitude, margin < DELTA);
    }

    public void testPointsStartCorrectly() throws Exception {
        Assert.fail("Not yet implementing");
    }

    public void testValidTilt() throws Exception {
        float last = -1;
        boolean wasSameLastTime = false;
        //Must be between 0 and 90 to be proper degress
        for (int i = 0; i < 10000; i++) {
            float tilt = mChooser.randomTilt();
            assertTrue("The tilt fell outside of the range " + MIN_TILT + "-" + MAX_BEARING + ", it was: " + tilt,
                    MIN_TILT <= tilt && tilt <= MAX_TILT);
            boolean isSameNow = tilt == last;
            Log.d(TAG, "Current tilt: " + tilt + " last was: " + last);
            assertFalse("We've had a few duplicate values in the tilts, the function may not be returning unique values each time", wasSameLastTime && isSameNow);
            wasSameLastTime = isSameNow;
        }
    }

    public void testValidBearing() throws Exception {
        float last = -1;
        boolean wasSameLastTime = false;
        for (int i = 0; i < 10000; i++) {
            float bearing = mChooser.randomBearing();
            assertTrue("The bearing fell outside of the range " + MIN_BEARING + "-" + MAX_BEARING + ", it was: " + bearing,
                    MIN_BEARING <= bearing && bearing <= MAX_BEARING);
            boolean isSameNow = bearing == last;
            Log.d(TAG, "Current bearing: " + bearing + " last was: " + last);
            assertFalse("We've had a few duplicate values in the bearings, the function may not be returning unique values each time", wasSameLastTime && isSameNow);
            wasSameLastTime = isSameNow;
        }
    }


}
