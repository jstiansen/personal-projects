package com.example.edu.testversion.database;

import android.content.Context;
import android.test.AndroidTestCase;
import android.test.RenamingDelegatingContext;

import com.example.edu.testversion.activity.TestingHelpers;
import com.example.edu.testversion.activity.YelpGameApplication;
import com.example.edu.testversion.data.Category;

import java.util.Collection;
import java.util.Iterator;

import static com.google.android.apps.common.testing.ui.espresso.matcher.ViewMatchers.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

/**
 * Created by Jonathan Stiansen on 2014-08-18.
 */
public class DatabaseTests extends AndroidTestCase {
    private Collection<Category> TEST_CATEGORIES = getTestCategories();
    private CategoriesOperations dbOperator;
//
//    public DatabaseTests() {
//        super(CategoryChooser.class);
//    }

    @Override
    public void setUp() throws Exception {
        super.setUp();
        //CategoryChooser mChooser = getActivity();
        //Context mTestContext = new RenamingDelegatingContext(mChooser.getApplicationContext(), "test_");
        // We are changing the name of the database so we can 'use' a database which is exclusively for this test case
        Context mTestContext = new RenamingDelegatingContext(getContext(), "test_");
        dbOperator = YelpGameApplication.getCategoryOperations(mTestContext);
        dbOperator.open();
    }

    @Override
    public void tearDown() throws Exception {
        dbOperator.removeAllEntries();
        dbOperator.close();
        super.tearDown();

    }

    public void testInitialConditions() throws Exception {
        assertThat("Looks like the dbService is null!", dbOperator, notNullValue());
    }


    public void testDatabaseIsEmpty() throws Exception {
        assert(dbOperator.getCategories().isEmpty());
    }

    public void testAddCategory() throws Exception {
        String name = "Indian";
        String alias = "indian";
        // When
        dbOperator.addCategory(new Category(name, alias));
        Collection<Category> actualCategories = dbOperator.getCategories();

        int rows = actualCategories.size();
        Iterator<Category> itr = actualCategories.iterator();
        Category actualCat = itr.next();
        String actualName = actualCat.getName();
        String actualAlias = actualCat.getAlias();
        // Expect
        assertThat("Not correct number of rows, should be 1",
                rows, is(1));
        assertTrue("Food Type is not in database, found: " + actualName + " with alias: " + actualAlias,
                actualName.equals(name) && actualAlias.equals(alias));
    }

    public void testAddCategories() throws Exception {
        //when
        dbOperator.addCategories(TEST_CATEGORIES);

        Collection<Category> actualCategories = dbOperator.getCategories();
        int actualSize = actualCategories.size();
        int expectedSize = TEST_CATEGORIES.size();
        //then
        assertEquals("Both inputted and taken out categories should be same length, but are: " +actualCategories + " vs. expected: "+expectedSize,
                actualSize, expectedSize);
        assertTrue("Categories didn't add successfully", actualCategories.containsAll(TEST_CATEGORIES));
    }
//
//    // Depends on addCategory working
    public void testRemoveCategory_WithSameNameAndAlias() throws Exception {
        //setup
        Category category = new Category("Fake", "something");
        dbOperator.addCategory(category);
        //when
        Category catWithSameNameAndAlias = new Category("Fake","something");
        int categoriesRemoved = dbOperator.removeCategory(catWithSameNameAndAlias);
        //Then
        assertThat("Database is not removing categories",
                categoriesRemoved, is(1));
    }

    private Collection<Category> getTestCategories() {
        return TestingHelpers.getTestCategories();
    }

}
