package com.example.edu.testversion.activity;

import android.content.Context;
import android.test.ActivityInstrumentationTestCase2;

import com.example.edu.testversion.R;
import com.example.edu.testversion.data.Category;
import com.example.edu.testversion.testUtils.Primer;

import junit.framework.Assert;

import java.util.ArrayList;

import static com.google.android.apps.common.testing.ui.espresso.Espresso.onView;
import static com.google.android.apps.common.testing.ui.espresso.Espresso.openActionBarOverflowOrOptionsMenu;
import static com.google.android.apps.common.testing.ui.espresso.action.ViewActions.click;
import static com.google.android.apps.common.testing.ui.espresso.assertion.ViewAssertions.matches;
import static com.google.android.apps.common.testing.ui.espresso.matcher.ViewMatchers.assertThat;
import static com.google.android.apps.common.testing.ui.espresso.matcher.ViewMatchers.isDisplayed;
import static com.google.android.apps.common.testing.ui.espresso.matcher.ViewMatchers.withId;
import static com.google.android.apps.common.testing.ui.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.number.OrderingComparisons.greaterThan;

/**
 * Created by Jonathan Stiansen on 2014-08-12.
 */
public class CategoryChooserTest extends ActivityInstrumentationTestCase2<CategoryChooser> {

    private static final String TAG = "TEST_CategoryChooser";

    private CategoryChooser mChooser;
    private ArrayList<Category> TEST_CATEGORIES = TestingHelpers.getTestCategories();

    public CategoryChooserTest() {
        super(CategoryChooser.class);
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();
        //This will 'prime' the android app and stop animations which is neccessary for espresso tests.
        Primer turnOffAnimationsPrimer = new Primer();
    }


    /**
     * This is a work around for testing, so that we can set mock objects
     */
    public void setUpActivities() {
        mChooser = getActivity();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();

    }

    public void testInitialCondition() throws Exception {
        setUpActivities();
        assertNotNull("CategoryChooser was null, and should be running", mChooser);
    }

    public void testKeywordMenuAvailable() throws Exception {
        setUpActivities();

        openActionBarOverflowOrOptionsMenu(mChooser.getApplicationContext());

        onView(withText(R.string.keyword_menu_title))
                .check(matches(isDisplayed()));
    }

    public void testGeoMenuAvailable() throws Exception {
        setUpActivities();

        openActionBarOverflowOrOptionsMenu(mChooser.getApplicationContext());

        onView(withText(R.string.geographical_menu_title))
                .check(matches(isDisplayed()));
    }

    public void testCategoryMenuAvailable() throws Exception {
        setUpActivities();

        openActionBarOverflowOrOptionsMenu(mChooser.getApplicationContext());

        onView(withText(R.string.category_menu_title))
                .check(matches(isDisplayed()));
    }

    public void testCategorySpinnerAvailable() throws Exception {
        setUpActivities();
        startUpCategoryGame(mChooser.getApplicationContext());

//        onView(withId(R.id.category_choice))
//                .perform(click());
        onView(withId(R.id.game_spinner))  // withId(R.id.my_view) is a ViewMatcher
                .check(matches(isDisplayed())); // matches(isDisplayed()) is a ViewAssertion
    }

    public void testThatCategorySpinnerPopulates() throws Exception {
        setUpActivities();
        startUpCategoryGame(mChooser.getApplicationContext());

        onView(withId(R.id.game_spinner))      // withId(R.id.my_view) is a ViewMatcher
                .perform(click());
        assertThat("Spinner didn't populate with enough categories", mChooser.mSpinner.getAdapter().getCount(), greaterThan(1));

    }
//
//    public void testDialogDisplayed() throws Exception {
//        testThatCategorySpinnerPopulates();
//        onView(withId(R.id.restaurant_name_display))
//                .perform(click());
//
//        String name = "Vij's";
//        String dialogText = "Can you find where " + name + " is?";
//        //After button on click -- once runnable is completed
//        onView(withId(R.id.play_game_button))
//                .perform(click());
//        //Confirm play button unavailable
//        //Confirm dialog is there
//
//    }

//    public void testIntentSentWithCategory() throws Exception {
//        CategoriesOperations catOps = mock(CategoriesOperations.class);
//        YelpAPI yelpApi = mock(YelpAPI.class);
//        YelpDataParser parser = mock(YelpDataParser.class);
//
//        YelpGameApplication.setYelpAPI(yelpApi);
//        YelpGameApplication.setCategoriesOperations(catOps);
//        setUpActivities();
//        clickOverflowMenu(mChooser.getApplicationContext());
//
//        when(catOps.getCategories()).thenReturn(TEST_CATEGORIES);
//        when(yelpApi.queryAPI(anyString(), anyString(), anyString(), anyInt()))
//                .thenReturn(new JSONObject());
//        when(parser.parseRestaurantData(Matchers.any(JSONObject.class)))
//                .thenReturn(TestingHelpers.getRestaurants());
//
//        //mChooser.setDatabaseOperator(catOps);
//        Category c1 = TEST_CATEGORIES.get(0);
//        //Category c2 = TEST_CATEGORIES.get(1);
//
//        onView(withId(R.id.category_spinner))
//                .perform(click());
//        onData(allOf(is(instanceOf(String.class)),
//                is(c1.getName())))
//                .perform(click());
//
//        verify(catOps, times(1)).getCategories();
//        verify(yelpApi, atLeastOnce()).searchByBusinessId(anyString());
//    }


    public void testGeographicalSpinner_displayed() throws Exception {
        Assert.fail("Not yet implemented");
//        setUpActivities();
//        clickOverflowMenu(mChooser.getApplicationContext());
//
//        onView(withId(R.id.geographical_choice))
//                .perform(click());
//        onView(withId(R.id.geographical_spinner))  // withId(R.id.my_view) is a ViewMatcher
//                .check(matches(isDisplayed())); // matches(isDisplayed()) is a ViewAssertion
//        onView(withId(R.id.play_geo_game_button))
//                .check(matches(isDisplayed()));
    }

    public void testKeywordSpinner_displayed() throws Exception {
        Assert.fail("Not yet implemented");
//        setUpActivities();
//        clickOverflowMenu(mChooser.getApplicationContext());
//
//        onView(withId(R.id.keyword_choice))
//                .perform(click());
//        onView(withId(R.id.keyword_spinner))  // withId(R.id.my_view) is a ViewMatcher
//                .check(matches(isDisplayed())); // matches(isDisplayed()) is a ViewAssertion
//        onView(withId(R.id.play_keyword_game_button))
//                .check(matches(isDisplayed()));
    }

    public static void clickOverflowMenu(Context context){
        openActionBarOverflowOrOptionsMenu(context);
    }

    public static void startUpCategoryGame(Context context){
        clickOverflowMenu(context);
        onView(withText(R.string.category_menu_title))
                .perform(click());
    }
}
