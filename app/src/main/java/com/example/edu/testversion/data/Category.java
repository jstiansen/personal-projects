package com.example.edu.testversion.data;

/**
 * Created by Jonathan Stiansen on 2014-08-08.
 */
public class Category {
    private String name;
    private String alias;

    public Category(String name, String alias) {
        this.name = name;
        this.alias = alias;
    }

    public String getName() {
        return name;
    }

    // A category is equal if it has the same name.
    @Override
    public boolean equals(Object o) {
        return o instanceof Category &&
                this.name.equals(((Category) o).getName()) &&
                this.alias.equals(((Category) o).getAlias());
    }

    public String getAlias() {
        return alias;
    }
}
