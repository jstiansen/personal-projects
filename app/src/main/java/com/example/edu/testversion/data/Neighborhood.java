package com.example.edu.testversion.data;

/**
 * Created by Jonathan Stiansen on 2014-09-06.
 */
public class Neighborhood implements GeoArea {

    private String neighborhoodName;
    private City city;

    public Neighborhood(String neighborhoodName, City city) {
        this.neighborhoodName = neighborhoodName;
        this.city = city;
    }

    @Override
    public String getCityString() {
        return city.getCityString();
    }

    /**
     * Returns the name different neighbourhood names, starting with just the first word, then first plus second
     * E.g. Neighborhood name: Fancy Slopes Drive -> String would
     * first -> Fancy, City, Prov,
     * second -> Fancy Slopes, City, Prov,
     * Third -> Fancty Slopes Drive, City, Prov
     * @return
     */
    @Override
    public String[] getAllGeoStrings(){
        String[] neighborhoodNames = neighborhoodName.split(" ");
        for(int i = neighborhoodNames.length - 1; i >= 0 ; --i){
            if (i == 0)
                neighborhoodNames[i] = neighborhoodNames[i] + ", " + city.getCityString();
            else
                neighborhoodNames[i] =
                        neighborhoodNames[i-1]   + " " +
                        neighborhoodNames[i] + ", " +
                                city.getCityString();
        }
        return  neighborhoodNames;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Neighborhood that = (Neighborhood) o;

        if (!city.equals(that.city)) return false;
        if (!neighborhoodName.equals(that.neighborhoodName)) return false;
        return true;
    }
}
