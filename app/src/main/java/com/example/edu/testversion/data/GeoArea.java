package com.example.edu.testversion.data;

/**
 * Created by Jonathan Stiansen on 2014-09-05.
 */
public interface GeoArea {

    String getCityString();

    /**
     * Returns all different geographic strings, will be size 1 for cities and possibly more for
     * neighbourhoods
     * @return An array of different geographical strings, that may be recognized for the area
     */
    String[] getAllGeoStrings();
}
