package com.example.edu.testversion.yelp;

import android.util.Log;

import com.example.edu.testversion.data.Category;
import com.example.edu.testversion.data.City;
import com.example.edu.testversion.data.GeoArea;
import com.example.edu.testversion.data.Neighborhood;
import com.example.edu.testversion.data.Restaurant;
import com.example.edu.testversion.data.Review;
import com.example.edu.testversion.yelp.exceptions.ConversionException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Jonathan Stiansen on 2014-08-18.
 */
public class YelpDataParser {

    private static final String TAG = "YelpDataParser";

    public static List<Review> convertReviews(JSONArray reviewsJSON) {
        ArrayList<Review> reviews = new ArrayList<Review>();
        for (Object r : reviewsJSON) {
            JSONObject review = (JSONObject) r;
            Long rating = (Long) review.get("rating");
            String excerpt = (String) review.get("excerpt");
            String id = (String) review.get("id");
            Long timeCreated = (Long) review.get("time_created");
            String ratingImageURL = (String) review.get("rating_image_small_url");
            reviews.add(new Review(id, rating, excerpt, timeCreated, ratingImageURL));
        }
        return reviews;
    }

    /**
     * This method will take a Yelp API - JSON Object in string form, and parse it to the corresponding list of restaurants
     *
     * @param yelpSearchResponse
     * @return A list of all restaurants inside the JSON response
     * @throws ParseException      When the syntax for the JSON object's syntax is incorrect
     * @throws ConversionException When the corresponding JSONObject doesn't follow the schema for our restaurant objects
     */
    public List<Restaurant> parseRestaurantData(JSONObject yelpSearchResponse) throws ParseException, ConversionException {

        JSONArray businesses = (JSONArray) yelpSearchResponse.get("businesses");

        ArrayList<Restaurant> restaurants = new ArrayList<Restaurant>();

        for (Object bus : businesses) {
            JSONObject business = (JSONObject) bus;
            //Create the restaurant
            Boolean isClosed = (Boolean) business.get("is_closed");
            if (!isClosed) {

                JSONObject locationObj = (JSONObject) business.get("location");
                // We only want results that are from Vancouver right now, since restaurant locations require neighbourhoods and neighbourhoods only exist in Vancouver
                if (locationObj.get("city").equals("Vancouver")) {
                    JSONArray categoriesJSON = (JSONArray) business.get("categories");
                    Collection<Category> categories = convertCategories(categoriesJSON);
                    String id = business.get("id").toString();
                    String name = business.get("name").toString();

                    // Postal Code
                    String postalCode = (String) locationObj.get("postal_code");
                    // Street Address
                    JSONArray addressArr = (JSONArray) locationObj.get("address");
                    String address = (String) addressArr.get(0);
                    // Location
                    GeoArea geoArea = convertGeoArea(locationObj);
                    //Reviews
                    Object reviewObj = business.get("reviews");
                    List<Review> reviews;
                    // Unless we do a business search review will be null
                    if (reviewObj != null) {
                        reviews = convertReviews((JSONArray) reviewObj);
                    } else {
                        reviews = new ArrayList<Review>();
                    }
                    Restaurant restaurant = new Restaurant(name, categories, id, address, postalCode, geoArea, reviews);
                    restaurants.add(restaurant);
                }

            }
        }
        Log.d(TAG, "**** There are " + restaurants.size() + " restaurants " + " and the object is " + restaurants.toString());
        return restaurants;
    }

    protected JSONObject parseObject(String jsonResponse) throws ParseException {
        JSONParser parser = new JSONParser();
        JSONObject response = null;
        try {
            response = (JSONObject) parser.parse(jsonResponse);
        } catch (ParseException pe) {
            Log.e(TAG, "Error: could not parseObject JSON response:");
            Log.e(TAG, "The response was: " + jsonResponse);
            throw pe;
        }
        return response;
    }

    /**
     * Takes JSON array and converts it into a collection of categories
     *
     * @param categories must be a JSON array,
     * @return a collection of the JSON array's categories made into objects
     * @throws ParseException
     * @throws ConversionException
     */
    protected Collection<Category> convertCategories(JSONArray categories) throws ParseException, ConversionException {
        ArrayList<Category> returnCats = new ArrayList<Category>();
        for (Object o : categories) {
            if (!(o instanceof JSONArray))
                throw new ConversionException("Category array did not contain other category arrays,\n" +
                        "so could not convert to a category");

            JSONArray jsonCategory = (JSONArray) o;
            if (jsonCategory.size() != 2) {
                Log.e(TAG, "categories failed to parseObject, based on the size of: " + jsonCategory.size() + ", should be 2");
                throw new ConversionException("Categories could not be parsed based on the input string, " +
                        "\nit might not have alias and name");
            }
            Category category = new Category((String) jsonCategory.get(0), (String) jsonCategory.get(1));
            returnCats.add(category);
        }
        return returnCats;
    }

    protected GeoArea convertGeoArea(JSONObject jsonObject) {
        String cityName = (String) jsonObject.get("city");
        JSONArray neighborhoods = (JSONArray) jsonObject.get("neighborhoods");
        String stateCode = (String) jsonObject.get("state_code");
        String countryCode = (String) jsonObject.get("country_code");
        City city = new City(cityName, stateCode, countryCode);
        if (neighborhoods == null || neighborhoods.size() == 0) {
            return city;
        }
        return new Neighborhood((String) neighborhoods.get(0), city);
    }
}
