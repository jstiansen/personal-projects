package com.example.edu.testversion.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.edu.testversion.R;
import com.example.edu.testversion.data.Category;

import java.util.List;

/**
 * Created by Jonathan Stiansen on 2014-09-09.
 */
public class CategoryAdapter extends ArrayAdapter<Category> {
    private  List<Category> objects;

    public CategoryAdapter(Context context, int resource, List<Category> objects) {
        super(context, resource, objects);
        this.objects = objects;
    }

    @Override
    public View getDropDownView(int position, View cnvtView, ViewGroup prnt) {
        return getCustomView(position, cnvtView, prnt);
    }

    @Override
    public View getView(int pos, View cnvtView, ViewGroup prnt) {
        return getCustomView(pos, cnvtView, prnt);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {

        // assign the view we are converting to a local variab
        // first check to see if the view is null. if so, we have to inflate it.
        // to inflate it basically means to render, or show, the view.
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.support_simple_spinner_dropdown_item, null);
            convertView.setTag(convertView.findViewById(android.R.id.text1));
        }

		/*
		 * Recall that the variable position is sent in as an argument to this method.
		 * The variable simply refers to the position of the current object in the list. (The ArrayAdapter
		 * iterates through the list we sent it)
		 *
		 * Therefore, i refers to the current Item object.
		 */
        Category c = objects.get(position);

        TextView tv = (TextView) convertView.getTag();
        tv.setText(c.getName());


        // the view must be returned to our activity
        return tv;
    }

}
