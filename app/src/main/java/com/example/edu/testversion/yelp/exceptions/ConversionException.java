package com.example.edu.testversion.yelp.exceptions;

/**
 * Created by Jonathan Stiansen on 2014-08-19.
 */
public class ConversionException extends Exception {

    public ConversionException(String detailMessage) {
        super(detailMessage);
    }
}
