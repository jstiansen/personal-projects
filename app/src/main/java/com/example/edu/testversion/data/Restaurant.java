package com.example.edu.testversion.data;

import java.util.Collection;
import java.util.List;

/**
 * Created by Jonathan Stiansen on 2014-08-08.
 */
public class Restaurant {

    private String address;
    private GeoArea geographicalArea;
    private String name;
    private Collection<Category> categories;
    private String id;
    private String postalCode;
    private List<Review> reviews;

    public Restaurant(String name, Collection<Category> categories, String id, String address, String postalCode, GeoArea geographicalArea, List<Review> reviews) {
        this.name = name;
        this.categories = categories;
        this.id = id;
        this.address = address;
        this.geographicalArea = geographicalArea;
        this.postalCode = postalCode;
        this.reviews = reviews;
    }

    public String getAddress() {
        return address;
    }

    public List<Review> getReviews() {
        return reviews;
    }

    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }

    public String getName() {
        return name;
    }

    public Collection<Category> getCategories() {
        return categories;
    }

    public String getId() {
        return id;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public GeoArea getGeographicalArea() {
        return geographicalArea;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Restaurant that = (Restaurant) o;

        if (address != null ? !address.equals(that.address) : that.address != null) return false;
        if (!categories.equals(that.categories)) return false;
        if (!geographicalArea.equals(that.geographicalArea)) return false;
        if (!id.equals(that.id)) return false;
        if (!name.equals(that.name)) return false;
        if (postalCode != null ? !postalCode.equals(that.postalCode) : that.postalCode != null)
            return false;

        return true;
    }

    @Override
    public String toString() {
        return name + ", " + geographicalArea.getAllGeoStrings()[0] + ", " + this.getPostalCode();
    }

    public int hasKeyword(KeyWord kw) {
        int numOfKeyword = 0;
        for (Review r : reviews) {
            numOfKeyword += r.hasKeyword(kw.word);
        }
        return numOfKeyword;

    }
}