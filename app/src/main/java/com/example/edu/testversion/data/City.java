package com.example.edu.testversion.data;

/**
 * Created by Jonathan Stiansen on 2014-09-05.
 */
public class City implements GeoArea {

    private String provinceOrStateCode;
    private String city;
    private String countryCode;

    @Override
    public String getCityString() {
        return city + ", " + provinceOrStateCode + ", "+countryCode;
    }

    @Override
    public String[] getAllGeoStrings() {
        return new String[]{ getCityString() };
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        City c = (City) o;

        if (!city.equals(c.city)) return false;
        if (!countryCode.equals(c.countryCode)) return false;
        if (!provinceOrStateCode.equals(c.provinceOrStateCode)) return false;

        return true;
    }

    public City(String cityName, String province, String countryCode) {
        if ( countryCode.length() != 2 || province.length() != 2)
            throw new IllegalArgumentException("Size of province and country code must be exactly 2, country was :" + countryCode + ", province was: "+province);
        this.provinceOrStateCode = province;
        this.city = cityName;
        this.countryCode = countryCode;
    }

}
