package com.example.edu.testversion.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static com.example.edu.testversion.database.CategoryReaderContract.CategoryEntry;

/**
 * Created by Jonathan Stiansen on 2014-08-18.
 */
public class CategoryDatabase extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASENAME = "CategoryReader.db";



    public CategoryDatabase(Context context) {
        // When entering null for cursor value, we are specifying that we want the stardard cursor
        // behavior not a custom behavior
        super(context, DATABASENAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ", ";
    private static final String NOT_NULL = "NOT NULL";
    private static final String SQL_CREATE_ENTRIES =
            // Start Create Table string
            "CREATE TABLE " + CategoryEntry.TABLE_NAME + " (" +
                    // NOT NULL is needed in sqlite since it allows null values as primary keys
                    CategoryEntry.CATEGORY_NAME + " " + TEXT_TYPE + " " + NOT_NULL + ", "+
                    CategoryEntry.CATEGORY_ALIAS + " " + TEXT_TYPE + " "  + NOT_NULL + ",  "+
                    "PRIMARY KEY (" + CategoryEntry.CATEGORY_NAME + COMMA_SEP + CategoryEntry.CATEGORY_ALIAS + " )" +

                    // end create table string.
                    ");";

    protected static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + CategoryEntry.TABLE_NAME +";";
}
