package com.example.edu.testversion.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.edu.testversion.activity.YelpGameApplication;
import com.example.edu.testversion.data.Category;
import com.example.edu.testversion.data.Restaurant;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Add remove database is a class which creates an interface to more easily use the database, so that nothing in the database is exposed
 * Removal is currently un-neccesary.
 * <p/>
 * Created by Jonathan Stiansen
 */
public class CategoriesOperations {
    // I'm using the subclass to remove my dependancy on Category Database, this way if
    // I want to use another database later, I don't have ot change ANY code out here, I just
    // implement the database following the template offered by SQLiteOpenHelper
    private ArrayList<Restaurant> recentRestaurants;
    private SQLiteOpenHelper dbHelper;
    private String[] CATEGORY_COLUMNS = {CategoryReaderContract.CategoryEntry.CATEGORY_NAME,
            CategoryReaderContract.CategoryEntry.CATEGORY_ALIAS};
    private SQLiteDatabase db;

    public CategoriesOperations(Context context) {
        dbHelper = YelpGameApplication.getDatabaseHelper(context);
        recentRestaurants = new ArrayList<Restaurant>();
    }

    public void open() {

        db = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public Collection<Category> getCategories() {
        // Cursor has whole table
        Cursor cursor = db.query(CategoryReaderContract.CategoryEntry.TABLE_NAME, CATEGORY_COLUMNS, null, null, null, null, null);
        ArrayList<Category> categories = new ArrayList<Category>();

        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            Category category = parseCategory(cursor);
            assert(category != null);
            categories.add(category);
            boolean cursorMoved = cursor.moveToNext();
        }
        cursor.close();
        return categories;
    }

    public void addCategory(Category c) {
        ContentValues values = new ContentValues();
        values.put(CATEGORY_COLUMNS[0], c.getName());
        values.put(CATEGORY_COLUMNS[1], c.getAlias());

        long id = db.insert(CategoryReaderContract.CategoryEntry.TABLE_NAME,
                null,
                values);
//        ArrayList<Category> categories = new ArrayList<Category>();
//        categories.add(c);
//        addCategories(categories);
    }


    public void addCategories(Collection<Category> categories) {
        for(Category c: categories){
            addCategory(c);
        }
    }


    public int removeCategory(Category c) {
        return db.delete(CategoryReaderContract.CategoryEntry.TABLE_NAME,
                CategoryReaderContract.CategoryEntry.CATEGORY_NAME + " = ? ",
                new String[]{c.getName()});
    }

    /**
     * This is primarily for testing the table, don't use unless you are very sure of what you are doing
     */
    public void removeAllEntries() {
        db.execSQL("DROP TABLE IF EXISTS " + CategoryReaderContract.CategoryEntry.TABLE_NAME +";");
    }

    private Category parseCategory(Cursor cursor){
        String name = cursor.getString(0);
        String alias = cursor.getString(1);
        return new Category(name, alias);
    }
}
