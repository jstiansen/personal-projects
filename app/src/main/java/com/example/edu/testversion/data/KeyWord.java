package com.example.edu.testversion.data;

/**
 * Created by Jonathan Stiansen on 2014-09-05.
 */
public class KeyWord {

    public KeyWord(String word) {
        this.word = word;
    }

    String word;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        KeyWord keyWord = (KeyWord) o;

        if (word != null ? !word.equals(keyWord.word) : keyWord.word != null) return false;

        return true;
    }
}
