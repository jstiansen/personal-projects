package com.example.edu.testversion.data;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Jonathan Stiansen on 2014-09-05.
 */
public class Review {

    public String getReview() {
        return review;
    }
    public java.util.Set<String> getKeywords(){
        return keywords.keySet();
    }

    private String id;
    private long rating;
    String review;
    private long timeCreated;
    private String ratingImageURL;
    Map<String, Integer> keywords;

    public Review(String id, long rating, String reviewString, long timeCreated, String ratingImageURL) {
        this.id = id;
        this.rating = rating;
        this.review = reviewString;
        this.timeCreated = timeCreated;
        this.ratingImageURL = ratingImageURL;
        keywords = new HashMap<String, Integer>();
        convertReviewToKeywords(review);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Review r = (Review) o;

        if (rating != r.rating) return false;
        if (timeCreated != r.timeCreated) return false;
        if (!id.equals(r.id)) return false;
        if (!keywords.equals(r.keywords)) return false;
        if (ratingImageURL != null ? !ratingImageURL.equals(r.ratingImageURL) : r.ratingImageURL != null)
            return false;
        if (!review.equals(r.review)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + (int) (rating ^ (rating >>> 32));
        result = 31 * result + review.hashCode();
        result = 31 * result + (int) (timeCreated ^ (timeCreated >>> 32));
        result = 31 * result + (ratingImageURL != null ? ratingImageURL.hashCode() : 0);
        result = 31 * result + keywords.hashCode();
        return result;
    }

    private void convertReviewToKeywords(String review) {
        String delim = "[ .,?!]+";
        String[] tokens = review.split(delim);
        for (String kw : tokens) {
            Integer kwValue = keywords.get(kw);
            keywords.put(kw, kwValue == null ? 1 : ++kwValue);
        }
    }

    public int hasKeyword(String kw) {
        Integer kwNum = keywords.get(kw);
        return kwNum == null ? 0 : kwNum;
    }
}
