package com.example.edu.testversion.activity;

import android.app.ActionBar;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.edu.testversion.R;
import com.example.edu.testversion.data.Category;
import com.example.edu.testversion.data.GeoArea;
import com.example.edu.testversion.data.KeyWord;
import com.example.edu.testversion.data.Restaurant;
import com.example.edu.testversion.data.Review;
import com.example.edu.testversion.util.CategoryAdapter;
import com.example.edu.testversion.yelp.YelpAPI;
import com.example.edu.testversion.yelp.YelpDataParser;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.SphericalUtil;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;


public class CategoryChooser extends ActionBarActivity {
    // Constants
    private static final String TAG = "CategoryChooser";
    private static final int INIT_POINTS = 1000;
    private static final int INCREMENT = 5;
    private Random rand;
    // Objects being used
    private GoogleMap mMap;
    private YelpAPI mYelpApi;
    final Handler mainThreadHandler = new Handler();
    //Game stuff
    private Geocoder geocoder;
    protected Spinner mSpinner;
    private int points;
    private TextView pointsView;
    private List<Restaurant> recentRestaurants;

    private boolean isDownloadingDone = true;
    private Button mPlayButton;
    private TextView mRestDialog;
    private Timer myTimer;
    private View initialDialog;
    private boolean isReviewDownloadsDone;
    private AutoCompleteTextView mAutocompleteLocation;
    private String autoCompleteSelectedItem;
    //TODO: Remove for students
    private static final String API_KEY =  "AIzaSyAfObqQyvhGQs8_QjiU-B4b8O6Ee3Z2Zmo";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_chooser);
        mYelpApi = YelpGameApplication.getYelpApi();
        rand = new Random(45);
        mSpinner = (Spinner) findViewById(R.id.game_spinner);
        mPlayButton = (Button) findViewById(R.id.play_game_button);
        mAutocompleteLocation = (AutoCompleteTextView) findViewById(R.id.find_location_dropdown);

        Log.d(TAG, "Done initial setup");
        // Starts Yelp API Async search
        isDownloadingDone = false;
        isReviewDownloadsDone = false;
        new DownloadRestaurantsFromYelpTask().execute();
        new DownloadRestaurantReviewsFromYelpTask().execute();
        //setUpPlayButtonListener();
        setUpMap();


    }


    private void setUpMap() {
        Log.d(TAG, "Setting up Map");
        if (initializeMapObject()) {
            Log.d(TAG, "Map is set up");
            mMap.getUiSettings().setAllGesturesEnabled(false);
            mMap.getUiSettings().setZoomControlsEnabled(false);
        }
        if (Geocoder.isPresent()) {
            pointsView = (TextView) findViewById(R.id.points_text_view);
            geocoder = new Geocoder(this);
            Log.d(TAG, "Geocoder initialized");
        } else {
            Toast.makeText(this, "Sorry you actually don't have a geocoder initialized", Toast.LENGTH_LONG);
            Log.d(TAG, "Sorry you actually don't have a geocoder initialized");
        }
        Log.d(TAG, "Done setting up Map");

    }

    private boolean initializeMapObject() {
        if (!checkGooglePlayIsInstalledAndUpToDate())
            return false;

        mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map))
                .getMap();
        return true;
    }

    private boolean checkGooglePlayIsInstalledAndUpToDate() {
        int googlePlayServicesCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (!(googlePlayServicesCode == ConnectionResult.SUCCESS)) {
            int activityResultCode = 10;
            GooglePlayServicesUtil.getErrorDialog(googlePlayServicesCode, this, activityResultCode);
            return false;
        } else {
            return true;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.category_chooser, menu);
        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        actionBar.setDisplayShowTitleEnabled(false);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        initialDialog = findViewById(R.id.get_started_dialog);
        stopGame();
        mPlayButton = (Button) findViewById(R.id.play_game_button);
        mSpinner = (Spinner) findViewById(R.id.game_spinner);
        switch (item.getItemId()) {

            case R.id.category_choice:
                Log.d(TAG, "trying to start up category game");
                mAutocompleteLocation.setVisibility(View.GONE);
                mSpinner.setVisibility(View.VISIBLE);
                initialDialog.setVisibility(View.GONE);
                mPlayButton.setVisibility(View.VISIBLE);
                setUpCategorySpinner();
                setUpPlayButtonListener();
                return true;

            case R.id.geographical_choice:
                Log.d(TAG, "trying to start up geographical game");
                mSpinner.setVisibility(View.GONE);
                mAutocompleteLocation.setVisibility(View.VISIBLE);
                initialDialog.setVisibility(View.GONE);
                mPlayButton.setVisibility(View.VISIBLE);
                setUpLocationTextAndPlayButton();
                return true;

            case R.id.keyword_choice:
                Log.d(TAG, "trying to start up keyword game");
                mAutocompleteLocation.setVisibility(View.GONE);
                mSpinner.setVisibility(View.VISIBLE);
                initialDialog.setVisibility(View.GONE);
                mPlayButton.setVisibility(View.VISIBLE);
                setUpKeywordSpinner();
                setUpPlayButtonListener();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }




    /// *****************************************************************************************************************************
    ///
    /// Non-Intialization methods
    ///
    /// **************************************************************************************************************************** *
    private class DownloadRestaurantsFromYelpTask extends AsyncTask<Void, Integer, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            JSONObject jsonObject = mYelpApi.queryAPI("", "Vancouver,BC", "", 10);
            YelpDataParser parser = new YelpDataParser();
            try {
                List<Restaurant> restaurants = parser.parseRestaurantData(jsonObject);
                if (recentRestaurants == null) {
                    recentRestaurants = restaurants;
                } else recentRestaurants.addAll(restaurants);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            isDownloadingDone = true;
            Log.d(TAG, "************************FINISHED getting categories ***********");
        }
    }

    private class DownloadRestaurantReviewsFromYelpTask extends AsyncTask<Void, Integer, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            for (Restaurant r : recentRestaurants) {
                JSONObject businessString = mYelpApi.searchByBusinessId(r.getId());

                List<Review> reviews = YelpDataParser.convertReviews((JSONArray) businessString.get("reviews"));
                r.setReviews(reviews);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            isReviewDownloadsDone = true;
            Log.d(TAG, "************************FINISHED getting reviews ***********");

        }
    }

    private void setUpCategorySpinner() {
        CategoryAdapter adapter = new CategoryAdapter(this, android.R.layout.simple_spinner_item, getCategories());
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner.setAdapter(adapter);
        Log.d(TAG, "Done setting up cat spinner");
    }

    private ArrayList<Category> getCategories() {
        Log.d(TAG, "****** GETTING CATEGORIES **********");
        ArrayList<Category> categories = new ArrayList<Category>();
        while (!isDownloadingDone) {
            try {
                wait(10);
            } catch (InterruptedException e) { /* Do nothing with exception */}
            Log.d(TAG, "* WAIT *");
        }
        for (int i = 0; i < recentRestaurants.size(); i++) {
            categories.addAll(recentRestaurants.get(i).getCategories());
        }
        return categories;
    }

    private void setUpKeywordSpinner() {
        // If this type is changed from string -> Keyword is changed, the initgame/startgame needs to be changed too
        Collection<String> keywords = getKeywords();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, keywords.toArray(new String[keywords.size()]));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner.setAdapter(adapter);
        Log.d(TAG, "Done setting up keyword spinner");
    }

    private Collection<String> getKeywords() {
        Log.d(TAG, "****** GETTING KEYWORDS **********");
        Set<String> keywords = new HashSet<String>();
        while (!isReviewDownloadsDone) {
            try {
                wait(10);
            } catch (InterruptedException e) { /* Do nothing with exception */}
            Log.d(TAG, "* WAIT *");
        }
        for (int i = 0; i < recentRestaurants.size(); i++) {
            for (Review r : recentRestaurants.get(i).getReviews()) {
                keywords.addAll(r.getKeywords());
            }
        }
        return keywords;
    }

    private void setUpPlayButtonListener() {
        mPlayButton = (Button) findViewById(R.id.play_game_button);
        mPlayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "Button has been clicked, trying to send intent for game map");
                Object spinnerItem = mSpinner.getSelectedItem();
                startGame(spinnerItem);
            }
        });
    }


    private void startGame(Object spinnerItem) {
        Log.d(TAG, "Starting game!");
        Iterator<Restaurant> itr = recentRestaurants.iterator();
        Restaurant r = null;
        boolean hasItem = false;
        boolean showNeighbourhood = false;
        while (itr.hasNext()) {
            r = itr.next();
            if (spinnerItem instanceof Category) {

                hasItem = r.getCategories().contains(spinnerItem);

            } else if (spinnerItem instanceof String) {

                hasItem = r.hasKeyword(new KeyWord((String) spinnerItem)) > 0;

            } else if (spinnerItem instanceof GeoArea) {

                hasItem = r.getGeographicalArea().equals(spinnerItem);
                showNeighbourhood = true;

            }
            if (hasItem) break;
            r = null;
        }
        // If none had the category put it into the log
        if (r == null) {
            Log.d(TAG, "Couldn't properly find a restaurant that had that category");
            Toast.makeText(this, "Sorry, try a different category please", Toast.LENGTH_SHORT).show();
        } else {
            initGame(r, showNeighbourhood);
        }
    }

    private void initGame(Restaurant r, boolean showNeighbourhood) {
        Log.d(TAG, "Initializing Game 'stuff'!");

        GeoArea geoArea = r.getGeographicalArea();
        try {
            Address focusAddress = null;
            LatLng restaurantLatLng = null;
            // If its in a neighbourhood, but we aren't searching by geolocation, only focus on city
            if (!showNeighbourhood) {
                focusAddress = geocoder.getFromLocationName(geoArea.getCityString(), 1).get(0);

            } else {
                String[] geoString = geoArea.getAllGeoStrings();
                // The last string is the most specific, the first is the least,
                // we want the most specific match so we start with the last and stop if we find a match
                List<Address> matches;
                for (int i = geoString.length - 1; i >= 0; ++i) {
                    matches = geocoder.getFromLocationName(geoString[i], 1);
                    if (matches.size() > 0) {
                        focusAddress = matches.get(0);
                        break;
                    }
                }
            }

            // If its null we can't focus on it, and it means that it couldn't match our address
            // in the geocoder
            if (focusAddress != null) {

                Address restaurantAdd = geocoder.getFromLocationName(r.getAddress() + " ," + geoArea.getCityString(), 1).get(0);

                restaurantLatLng = new LatLng(restaurantAdd.getLatitude(), restaurantAdd.getLongitude());
                LatLngBounds.Builder builder = LatLngBounds.builder();
                LatLng geoAreaLatLng = new LatLng(focusAddress.getLatitude(), focusAddress.getLongitude());
                LatLng swVancouverLatLng = new LatLng(49.23356, -123.220378);
                LatLng neVancouverLatLng = new LatLng(49.295168, -123.023310);
                LatLngBounds vancouverBounds = new LatLngBounds(swVancouverLatLng, neVancouverLatLng);
                boolean inVancouver = vancouverBounds.contains(geoAreaLatLng);
                builder.include(geoAreaLatLng);
                if (!showNeighbourhood && inVancouver) {
                    builder.include(vancouverBounds.southwest);
                    builder.include(vancouverBounds.northeast);
                } else {
                    builder.include(new LatLng(geoAreaLatLng.latitude - .1, geoAreaLatLng.longitude - .1));
                    builder.include(new LatLng(geoAreaLatLng.latitude + .1, geoAreaLatLng.longitude + .1));
                }
                LatLngBounds bounds = builder.build();
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, 10);
                mMap.animateCamera(cameraUpdate);
                points = INIT_POINTS;
                // Start points
                startPointCountdown();
                // Display restaurant name
                setGameDialog(r.getName());

                //Get Restaurant LatLng

                setUpMapListener(restaurantLatLng);
            } else {
                Log.d(TAG, "***** FOCUS ADDRESS IS NULL! Can't find Geo location");
                Toast.makeText(this, "Sorry we couldn't get this geographical area", Toast.LENGTH_SHORT).show();
            }
        } catch (IOException e) {
            Log.d(TAG, "trying to get geocoded for " + r.getGeographicalArea());
        }
    }

    private void setUpMapListener(final LatLng restaurantLatLng) {
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                // Get restaurant latlng
                // Get distance between latlng and restaurant latlng
                // Is that distance sufficiently short?
                // if not zoom to a spot on the map -- centered on the click
                // half points
                double distance = SphericalUtil.computeDistanceBetween(latLng, restaurantLatLng);
                mRestDialog.setText(mRestDialog.getText() + "\nYour click was: " + ((int) distance) + " metres away");
                // if its short enough trigger game win!
                if (distance < 50) {
                    winGame(restaurantLatLng);
                    return;
                }
                LatLng swCorner = new LatLng(
                        latLng.latitude - (Math.abs(latLng.latitude - restaurantLatLng.latitude) * 1.5),
                        latLng.longitude - (Math.abs(latLng.longitude - restaurantLatLng.longitude) * 1.5));
                LatLng neCorner = new LatLng(
                        latLng.latitude + (Math.abs(latLng.latitude - restaurantLatLng.latitude) * 1.5),
                        latLng.longitude + (Math.abs(latLng.longitude - restaurantLatLng.longitude) * 1.5));

                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(
                        LatLngBounds.builder()
                                .include(swCorner)
                                .include(neCorner).build(), 10);

                points *= .75;
                mMap.animateCamera(cameraUpdate);
            }
        });
        Log.d(TAG, "Map listener is set up!");
    }

    private void winGame(LatLng restaurantLatLng) {
        Log.d(TAG, "You've won!");
        // Create popup!
        Integer score = Integer.parseInt(String.valueOf(pointsView.getText()));
        Toast toast = Toast.makeText(getApplicationContext(), "Congrats you just won! You earned an impressive " + score + " points!", Toast.LENGTH_LONG);
        //toast.setView(findViewById(R.id.baseDialog));
        toast.show();
        stopGame();
        mMap.addMarker(new MarkerOptions()
                .position(restaurantLatLng)
                .title("Here it is!"));
    }

    public void stopGame() {
        Log.d(TAG, "Stopping game");
        autoCompleteSelectedItem = "";
        if (myTimer != null) myTimer.cancel();
        // if(hasWon) initialDialog.set(mRestDialog.getText() + "\nHow do you want to play the next game?");
        if (mRestDialog != null)
            mRestDialog.setVisibility(View.GONE);

        pointsView.setText("");
        initialDialog.setVisibility(View.VISIBLE);
    }


    private void setGameDialog(String name) {
        mSpinner.setVisibility(View.GONE);
        mPlayButton.setVisibility(View.GONE);
        mRestDialog = (TextView) findViewById(R.id.restaurant_name_display);
        mRestDialog.setVisibility(View.VISIBLE);
        mRestDialog.setText("Where do you think " + name + " is?");
    }

    private void startPointCountdown() {
        myTimer = new Timer();
        myTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                updateGUI();
            }
        }, 0, 1000);
    }

    private void updateGUI() {
        points -= INCREMENT;
        mainThreadHandler.post(pointsRunnable);
    }

    final Runnable pointsRunnable = new Runnable() {
        public void run() {
            pointsView.setText(String.valueOf(points));
        }
    };


    /**
    * ------------ ------------ ------------ ------------ ------------ ------------ ------------ ------------ ------------ ------------ ------------ ------------
    **/
    // Since it doesn't use a spinner, we need to change how it works, thus a different listener
    private void setUpLocationTextAndPlayButton() {
        mPlayButton = (Button) findViewById(R.id.play_game_button);
        mAutocompleteLocation.setThreshold(3);
        PlacesAutoCompleteAdapter adapter = new PlacesAutoCompleteAdapter(this, R.layout.list_item);
        mAutocompleteLocation.setAdapter(adapter);

        mAutocompleteLocation.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                String str = (String) adapterView.getItemAtPosition(position);
                autoCompleteSelectedItem = str;
                Toast.makeText(getApplicationContext(), str, Toast.LENGTH_SHORT).show();
                // Since someone can click the play button when there is no text in the autocomplete
                // menu, we only set the click listener when a selection is made
                mPlayButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.d(TAG, "Button has been clicked, trying to send intent for game map");
                        startGame(autoCompleteSelectedItem);
                    }
                });
            }
        });


    }

    protected float randomTilt() {
        return rand.nextFloat() % 90;
    }

    protected float randomBearing() {
        return rand.nextFloat() % 360;
    }

    private static final String LOG_TAG = "ExampleApp";

    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";


    private ArrayList<String> autocomplete(String input) {
        ArrayList<String> resultList = null;
        String country = "ca";

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
            sb.append("&components=country:" + country);
            sb.append("&types=(cities)");
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));

            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Create a JSON object hierarchy from the results
            JSONParser parser = new JSONParser();
            JSONObject jsonObj = (JSONObject) parser.parse(jsonResults.toString());
            JSONArray predsJsonArray = (JSONArray) jsonObj.get("predictions");

            // Extract the Place descriptions from the results
            resultList = new ArrayList<String>(predsJsonArray.size());
            for (int i = 0; i < predsJsonArray.size(); i++) {
                resultList.add((String) ((JSONObject) predsJsonArray.get(i)).get("description"));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return resultList;
    }

    private class PlacesAutoCompleteAdapter extends ArrayAdapter<String> implements Filterable {
        private ArrayList<String> resultList;

        public PlacesAutoCompleteAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        @Override
        public int getCount() {
            return resultList.size();
        }

        @Override
        public String getItem(int index) {
            return resultList.get(index);
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        // Retrieve the autocomplete results.
                        resultList = autocomplete(constraint.toString());

                        // Assign the data to the FilterResults
                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, Filter.FilterResults results) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
            return filter;
        }
    }

}
