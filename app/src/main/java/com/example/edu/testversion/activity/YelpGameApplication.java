package com.example.edu.testversion.activity;

import android.app.Application;
import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.edu.testversion.database.CategoriesOperations;
import com.example.edu.testversion.database.CategoryDatabase;
import com.example.edu.testversion.yelp.YelpAPI;


/**
 * This class was created to show how to implement Dependancy injection in an android
 * application which helps with effective testing immensly.
 * Created by Jonathan Stiansen on 2014-08-28.
 *
 */
public class YelpGameApplication extends Application {


    static String CONSUMER_KEY = "-f-Sq9aUXS4qzY6ntd834w";
    static String CONSUMER_SECRET = "7nqHfMqTPFgE6Lw-JGY8JNQTV54";
    static String TOKEN = "TpWey7p0o3GXnu8BXRVEei8BQGz4B0Kv";
    static String TOKEN_SECRET = "XOWZX4hYbWuz4_bedHp-luq-_oY";

    public static void setYelpAPI(YelpAPI yelp) {
        yelpAPI = yelp;
    }

    public static void setCategoriesOperations(CategoriesOperations categoryOperations) {
        categoriesOperations = categoryOperations;
    }

    public static void setDb(SQLiteOpenHelper database) {
        db = database;
    }

    private static YelpAPI yelpAPI;
    private static CategoriesOperations categoriesOperations;
    private static SQLiteOpenHelper db;

    public static YelpAPI getYelpApi(){
        // This is another way of writing some If statements, it can be done as one line
        // If yelpApi is null
        return yelpAPI == null ?
                //then
             new YelpAPI(CONSUMER_KEY,CONSUMER_SECRET,TOKEN,TOKEN_SECRET) :
                //else
                yelpAPI;
    }

    public static CategoriesOperations getCategoryOperations(Context context){
       return categoriesOperations == null ?
               new CategoriesOperations(context) :
               categoriesOperations;
    }

    public static SQLiteOpenHelper getDatabaseHelper(Context context){
        return db == null ?
            new CategoryDatabase(context) :
                db;
    }
}
