package com.example.edu.testversion.database;

import android.provider.BaseColumns;

/**
 * Created by Jonathan Stiansen on 2014-08-18.
 */
final class CategoryReaderContract {

    /**
     * This should not be instantiated, i.e. you should never say "new CategoryReaderContract", since this class defines what the database should look like
     */
    public CategoryReaderContract() {}

    public static abstract class CategoryEntry implements BaseColumns{
        public static final String TABLE_NAME = "category";
        public static final String CATEGORY_NAME = "cname";
        public static final String CATEGORY_ALIAS = "alias";
    }
}
